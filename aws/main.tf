terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.50.0"
    }
  }
}

provider "aws" {
  region  = var.AWS_REGION
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "psono" {
  cidr_block = "10.68.0.0/16"
}

resource "aws_subnet" "psono1" {
  vpc_id     = aws_vpc.psono.id
  cidr_block = "10.68.10.0/24"
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_subnet" "psono2" {
  vpc_id     = aws_vpc.psono.id
  cidr_block = "10.68.20.0/24"
  availability_zone = data.aws_availability_zones.available.names[1]
}

resource "aws_security_group" "psono_postgres" {
  name        = "psono_postgres"
  description = "PostgreSQL access from within VPC"
  vpc_id      = aws_vpc.psono.id

  ingress {
    description      = "PostgreSQL access from within VPC"
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.psono.cidr_block]
  }
}

resource "aws_security_group" "psono_vpc_connector" {
  name        = "psono_vpc_connector"
  description = "Access from the VPC connector"
  vpc_id      = aws_vpc.psono.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

resource "aws_db_subnet_group" "psono" {
  name       = "psono"
  subnet_ids = [aws_subnet.psono1.id, aws_subnet.psono2.id]
}

resource "aws_db_instance" "psono" {
  identifier_prefix    = "psono"
  allocated_storage    = 10
  db_name              = var.DATABASES_DEFAULT_DATABASE
  engine               = "postgres"
  engine_version       = "14.5"
  instance_class       = "db.t3.micro"
  username             = var.DATABASES_DEFAULT_USER
  password             = var.DATABASES_DEFAULT_PASSWORD
  skip_final_snapshot  = true
  multi_az  = true
  db_subnet_group_name = aws_db_subnet_group.psono.id
  vpc_security_group_ids = [aws_security_group.psono_postgres.id]
  deletion_protection  = false # set to true
}

resource "aws_apprunner_vpc_connector" "psono" {
  vpc_connector_name = "psono"
  subnets            = [aws_subnet.psono1.id, aws_subnet.psono2.id]
  security_groups    = [aws_security_group.psono_vpc_connector.id]
}

resource "aws_apprunner_service" "psono" {
  service_name = "psono"

  depends_on = [aws_db_instance.psono]

  network_configuration {
    egress_configuration {
      egress_type       = "VPC"
      vpc_connector_arn = aws_apprunner_vpc_connector.psono.arn
    }
    ingress_configuration {
      is_publicly_accessible       = true
    }
  }

  source_configuration {
    image_repository {

      image_identifier      = "${var.AWS_ECR}/psono-combo:latest"
      image_repository_type = "ECR_PUBLIC"
      image_configuration {
        port = "80"
        
        runtime_environment_variables = {
          "PSONO_SECRET_KEY": var.SECRET_KEY,
          "PSONO_ACTIVATION_LINK_SECRET": var.ACTIVATION_LINK_SECRET
          "PSONO_DB_SECRET": var.DB_SECRET
          "PSONO_EMAIL_SECRET_SALT": var.EMAIL_SECRET_SALT
          "PSONO_PRIVATE_KEY": var.PRIVATE_KEY
          "PSONO_PUBLIC_KEY": var.PUBLIC_KEY
          "PSONO_DATABASES_DEFAULT_HOST": aws_db_instance.psono.address,
          "PSONO_DATABASES_DEFAULT_NAME": var.DATABASES_DEFAULT_DATABASE,
          "PSONO_DATABASES_DEFAULT_USER": var.DATABASES_DEFAULT_USER,
          "PSONO_DATABASES_DEFAULT_PASSWORD": var.DATABASES_DEFAULT_PASSWORD,
      #    "PSONO_EMAIL_BACKEND": "django.core.mail.backends.smtp.EmailBackend",
          "PSONO_MANAGEMENT_ENABLED": "True",
          "PSONO_HEALTHCHECK_TIME_SYNC_ENABLED": "False",
          "PSONO_DISABLE_CENTRAL_SECURITY_REPORTS": "False",
      #    "PSONO_YUBIKEY_CLIENT_ID": "",
      #    "PSONO_YUBIKEY_SECRET_KEY": "",
          "PSONO_WEB_CLIENT_URL": "https://${var.WEB_DOMAIN}",
          "PSONO_ALLOWED_DOMAINS": "*",
          "UWSGI_PROCESSES": "1",

          "PSONO_DISABLED": "False",
          "PSONO_MAINTENANCE_ACTIVE": "False",
          "PSONO_ALLOW_REGISTRATION": "False",
          "PSONO_ALLOW_LOST_PASSWORD": "True",
          "PSONO_ALLOWED_SECOND_FACTORS": "yubikey_otp, google_authenticator, duo, webauthn",
          "PSONO_ENFORCE_MATCHING_USERNAME_AND_EMAIL": "False",
          "PSONO_ALLOW_USER_SEARCH_BY_EMAIL": "False",
          "PSONO_ALLOW_USER_SEARCH_BY_USERNAME_PARTIAL": "False",
      #    "PSONO_DUO_INTEGRATION_KEY": "",
      #    "PSONO_DUO_SECRET_KEY": "",
      #    "PSONO_DUO_API_HOSTNAME": "",
          "PSONO_MULTIFACTOR_ENABLED": "False",
      #    "PSONO_REGISTRATION_EMAIL_FILTER": "",
          "PSONO_DISABLE_LAST_PASSWORDS": "0",
          "PSONO_FILESERVER_HANDLER_ENABLED": "False",
          "PSONO_FILES_ENABLED": "True",
          "PSONO_ACTIVATION_LINK_TIME_VALID": "2592001",
          "PSONO_DEFAULT_TOKEN_TIME_VALID": "86400",
          "PSONO_MAX_WEBCLIENT_TOKEN_TIME_VALID": "2592000",
          "PSONO_MAX_APP_TOKEN_TIME_VALID": "31536000",
          "PSONO_MAX_API_KEY_TOKEN_TIME_VALID": "600",
          "PSONO_RECOVERY_VERIFIER_TIME_VALID": "600",
          "PSONO_ALLOW_MULTIPLE_SESSIONS": "True",
          "PSONO_AUTO_PROLONGATION_TOKEN_TIME_VALID": "0",
          "PSONO_AUTHENTICATION_METHODS": "AUTHKEY,SAML",
          "PSONO_COMPLIANCE_ENFORCE_CENTRAL_SECURITY_REPORTS": "True",
          "PSONO_COMPLIANCE_CENTRAL_SECURITY_REPORT_SECURITY_RECURRENCE_INTERVAL": "2592001",
          "PSONO_COMPLIANCE_ENFORCE_2FA": "False",
          "PSONO_COMPLIANCE_DISABLE_EXPORT": "False",
          "PSONO_COMPLIANCE_DISABLE_DELETE_ACCOUNT": "False",
          "PSONO_COMPLIANCE_DISABLE_API_KEYS": "False",
          "PSONO_COMPLIANCE_DISABLE_EMERGENCY_CODES": "False",
          "PSONO_COMPLIANCE_DISABLE_RECOVERY_CODES": "False",
          "PSONO_COMPLIANCE_DISABLE_FILE_REPOSITORIES": "False",
          "PSONO_COMPLIANCE_DISABLE_LINK_SHARES": "False",
          "PSONO_COMPLIANCE_MIN_MASTER_PASSWORD_LENGTH": "12",
          "PSONO_COMPLIANCE_MIN_MASTER_PASSWORD_COMPLEXITY": "0",
          "PSONO_SAML_CONFIGURATIONS": "",
          "PSONO_MANAGEMENT_COMMAND_ACCESS_KEY": var.MANAGEMENT_COMMAND_ACCESS_KEY,
          "PSONO_WEBCLIENT_CONFIG_JSON": <<-EOT
            {
              "backend_servers": [{
                "title": "Psono",
                "domain": "${var.USER_DOMAIN}"
              }],
              "allow_custom_server": true,
              "allow_registration": true,
              "allow_lost_password": true,
              "authentication_methods": ["AUTHKEY", "LDAP"],
              "more_links": [{
                "href": "https://doc.psono.com/",
                "title": "DOCUMENTATION",
                "class": "fa-book"
              },{
                "href": "privacy-policy.html",
                "title": "PRIVACY_POLICY",
                "class": "fa-user-secret"
              },{
                "href": "https://www.psono.com",
                "title": "ABOUT_US",
                "class": "fa-info-circle"
              }]
            }
          EOT
          "PSONO_PORTAL_CONFIG_JSON": <<-EOT
          {
            "backend_servers": [{
              "title": "Psono",
                "domain": "${var.USER_DOMAIN}"
            }],
            "allow_custom_server": true,
            "allow_registration": true,
            "allow_lost_password": true,
            "authentication_methods": ["AUTHKEY", "LDAP"]
          }
          EOT
          "PSONO_EMAIL_FROM": "noreply@psono.app",
          "PSONO_HOST_URL": "https://${var.WEB_DOMAIN}/server",
      #    "PSONO_SPLUNK_HOST": ""
      #    "PSONO_SPLUNK_PORT": ""
      #    "PSONO_SPLUNK_TOKEN": ""
      #    "PSONO_SPLUNK_INDEX": ""
      #    "PSONO_SPLUNK_PROTOCOL": ""
      #    "PSONO_SPLUNK_VERIFY": ""
      #    "PSONO_SPLUNK_SOURCETYPE": ""
      #    "PSONO_LOGSTASH_TRANSPORT": ""
      #    "PSONO_LOGSTASH_HOST": ""
      #    "PSONO_LOGSTASH_PORT": ""
      #    "PSONO_LOGSTASH_SSL_ENABLED": ""
      #    "PSONO_LOGSTASH_SSL_VERIFY": ""
          "PSONO_OIDC_CONFIGURATIONS": "{}",
          "PSONO_LDAPGATEWAY_EXCLUSIVE_SECRETS": "True",
          "PSONO_LDAPGATEWAY": "[]",
          "PSONO_COMPLIANCE_DISABLE_UNMANAGED_GROUPS": "True",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_PASSWORD_LENGTH": "17",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_LETTERS_UPPERCASE": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_LETTERS_LOWERCASE": "abcdefghijklmnopqrstuvwxyz",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_NUMBERS": "0123456789",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_SPECIAL_CHARS": ",.-;:_#+*~!\"§$%&/()=?{[]}"
        }
      }
    }
    auto_deployments_enabled = false
  }

  tags = {
    Name = "psono-service"
  }
}