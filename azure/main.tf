terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.33.0"
    }
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "psono" {
  name     = "psono-resource-group"
  location = var.location
}

resource "azurerm_virtual_network" "psono" {
  name                = "${azurerm_resource_group.psono.name}-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.psono.location
  resource_group_name = azurerm_resource_group.psono.name
}

resource "azurerm_subnet" "psonodb" {
  name                 = "${azurerm_resource_group.psono.name}-subnet"
  resource_group_name  = azurerm_resource_group.psono.name
  virtual_network_name = azurerm_virtual_network.psono.name
  address_prefixes       = ["10.0.1.0/24"]
  service_endpoints    = ["Microsoft.Storage"]
  delegation {
    name = "fs"
    service_delegation {
      name = "Microsoft.DBforPostgreSQL/flexibleServers"
      actions = [
        "Microsoft.Network/virtualNetworks/subnets/join/action",
      ]
    }
  }
}

resource "azurerm_subnet" "psono" {
  name                 = "${azurerm_resource_group.psono.name}-service-subnet"
  resource_group_name  = azurerm_resource_group.psono.name
  virtual_network_name = azurerm_virtual_network.psono.name
  address_prefixes       = ["10.0.2.0/24"]
  delegation {
    name = "psono-service-delegation"

    service_delegation {
      name    = "Microsoft.Web/serverFarms"
      actions = ["Microsoft.Network/virtualNetworks/subnets/action"]
    }
  }
}

resource "azurerm_private_dns_zone" "psono" {
  name                 = "psono.postgres.database.azure.com"
  resource_group_name  = azurerm_resource_group.psono.name
}

resource "azurerm_private_dns_zone_virtual_network_link" "psono" {
  name                 = "${azurerm_resource_group.psono.name}-vnet-link"
  private_dns_zone_name = azurerm_private_dns_zone.psono.name
  virtual_network_id    = azurerm_virtual_network.psono.id
  resource_group_name   = azurerm_resource_group.psono.name
}

resource "azurerm_postgresql_flexible_server" "psono" {
  name                = "${azurerm_resource_group.psono.name}-postgres"
  resource_group_name = azurerm_resource_group.psono.name
  location            = azurerm_resource_group.psono.location
  version                = "14"
  delegated_subnet_id    = azurerm_subnet.psonodb.id
  private_dns_zone_id    = azurerm_private_dns_zone.psono.id
  administrator_login    = var.DATABASES_DEFAULT_USER
  administrator_password = var.DATABASES_DEFAULT_PASSWORD
  zone                   = "1"

  storage_mb = 32768

  sku_name   = "GP_Standard_D4s_v3"
  depends_on = [azurerm_private_dns_zone_virtual_network_link.psono]

  high_availability {
    mode                      = "ZoneRedundant"
    standby_availability_zone = "2"
  }
}

resource "azurerm_postgresql_flexible_server_configuration" "psono" {
  name      = "azure.extensions"
  server_id = azurerm_postgresql_flexible_server.psono.id
  value     = "PGCRYPTO,LTREE"
}

resource "azurerm_service_plan" "appserviceplan" {
  name                = "${azurerm_resource_group.psono.name}-plan"
  location            = azurerm_resource_group.psono.location
  resource_group_name = azurerm_resource_group.psono.name

  os_type  = "Linux"
  sku_name = "S1"

}

resource "azurerm_postgresql_flexible_server_database" "psono" {
  name      = "psono"
  server_id = azurerm_postgresql_flexible_server.psono.id
  collation = "en_US.utf8"
  charset   = "utf8"
}



resource "azurerm_linux_web_app" "psono" {
  name                = "${azurerm_resource_group.psono.name}-psono"
  location            = azurerm_resource_group.psono.location
  resource_group_name = azurerm_resource_group.psono.name
  service_plan_id     = azurerm_service_plan.appserviceplan.id

  virtual_network_subnet_id = azurerm_subnet.psono.id

  app_settings = {
    WEBSITES_ENABLE_APP_SERVICE_STORAGE = false
    PSONO_SECRET_KEY = var.SECRET_KEY
    PSONO_ACTIVATION_LINK_SECRET = var.ACTIVATION_LINK_SECRET
    PSONO_DB_SECRET = var.DB_SECRET
    PSONO_EMAIL_SECRET_SALT = var.EMAIL_SECRET_SALT
    PSONO_PRIVATE_KEY = var.PRIVATE_KEY
    PSONO_PUBLIC_KEY = var.PUBLIC_KEY
    PSONO_DATABASES_DEFAULT_HOST = "d3b547beb0c0.psono.postgres.database.azure.com."# "${azurerm_postgresql_flexible_server.psono.fqdn}"
    PSONO_DATABASES_DEFAULT_NAME = azurerm_postgresql_flexible_server_database.psono.name
    PSONO_DATABASES_DEFAULT_USER = azurerm_postgresql_flexible_server.psono.administrator_login
    PSONO_DATABASES_DEFAULT_PASSWORD = azurerm_postgresql_flexible_server.psono.administrator_password
#    PSONO_EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
    PSONO_MANAGEMENT_ENABLED = "True"
    PSONO_HEALTHCHECK_TIME_SYNC_ENABLED = "False"
    PSONO_DISABLE_CENTRAL_SECURITY_REPORTS = "False"
#    PSONO_YUBIKEY_CLIENT_ID = ""
#    PSONO_YUBIKEY_SECRET_KEY = ""
    PSONO_WEB_CLIENT_URL = "https://${var.WEB_DOMAIN}"
    PSONO_ALLOWED_DOMAINS = "*"
    UWSGI_PROCESSES = "1"

    PSONO_DISABLED = "False"
    PSONO_MAINTENANCE_ACTIVE = "False"
    PSONO_ALLOW_REGISTRATION = "False"
    PSONO_ALLOW_LOST_PASSWORD = "True"
    PSONO_ALLOWED_SECOND_FACTORS = "yubikey_otp, google_authenticator, duo, webauthn"
    PSONO_ENFORCE_MATCHING_USERNAME_AND_EMAIL = "False"
    PSONO_ALLOW_USER_SEARCH_BY_EMAIL = "False"
    PSONO_ALLOW_USER_SEARCH_BY_USERNAME_PARTIAL = "False"
#    PSONO_DUO_INTEGRATION_KEY = ""
#    PSONO_DUO_SECRET_KEY = ""
#    PSONO_DUO_API_HOSTNAME = ""
    PSONO_MULTIFACTOR_ENABLED = "False"
#    PSONO_REGISTRATION_EMAIL_FILTER = ""
    PSONO_DISABLE_LAST_PASSWORDS = "0"
    PSONO_FILESERVER_HANDLER_ENABLED = "False"
    PSONO_FILES_ENABLED = "True"
    PSONO_ACTIVATION_LINK_TIME_VALID = "2592001"
    PSONO_DEFAULT_TOKEN_TIME_VALID = "86400"
    PSONO_MAX_WEBCLIENT_TOKEN_TIME_VALID = "2592000"
    PSONO_MAX_APP_TOKEN_TIME_VALID = "31536000"
    PSONO_MAX_API_KEY_TOKEN_TIME_VALID = "600"
    PSONO_RECOVERY_VERIFIER_TIME_VALID = "600"
    PSONO_ALLOW_MULTIPLE_SESSIONS = "True"
    PSONO_AUTO_PROLONGATION_TOKEN_TIME_VALID = "0"
    PSONO_AUTHENTICATION_METHODS = "AUTHKEY,SAML"
    PSONO_COMPLIANCE_ENFORCE_CENTRAL_SECURITY_REPORTS = "True"
    PSONO_COMPLIANCE_CENTRAL_SECURITY_REPORT_SECURITY_RECURRENCE_INTERVAL = "2592001"
    PSONO_COMPLIANCE_ENFORCE_2FA = "False"
    PSONO_COMPLIANCE_DISABLE_EXPORT = "False"
    PSONO_COMPLIANCE_DISABLE_DELETE_ACCOUNT = "False"
    PSONO_COMPLIANCE_DISABLE_API_KEYS = "False"
    PSONO_COMPLIANCE_DISABLE_EMERGENCY_CODES = "False"
    PSONO_COMPLIANCE_DISABLE_RECOVERY_CODES = "False"
    PSONO_COMPLIANCE_DISABLE_FILE_REPOSITORIES = "False"
    PSONO_COMPLIANCE_DISABLE_LINK_SHARES = "False"
    PSONO_COMPLIANCE_MIN_MASTER_PASSWORD_LENGTH = "12"
    PSONO_COMPLIANCE_MIN_MASTER_PASSWORD_COMPLEXITY = "0"
    PSONO_SAML_CONFIGURATIONS = ""
    PSONO_MANAGEMENT_COMMAND_ACCESS_KEY = var.MANAGEMENT_COMMAND_ACCESS_KEY
    PSONO_WEBCLIENT_CONFIG_JSON = <<-EOT
      {
        "backend_servers": [{
          "title": "Psono.pw",
          "domain": "${var.USER_DOMAIN}"
        }],
        "allow_custom_server": true,
        "allow_registration": true,
        "allow_lost_password": true,
        "authentication_methods": ["AUTHKEY", "LDAP"],
        "more_links": [{
          "href": "https://doc.psono.com/",
          "title": "DOCUMENTATION",
          "class": "fa-book"
        },{
          "href": "privacy-policy.html",
          "title": "PRIVACY_POLICY",
          "class": "fa-user-secret"
        },{
          "href": "https://www.psono.com",
          "title": "ABOUT_US",
          "class": "fa-info-circle"
        }]
      }
    EOT
    PSONO_PORTAL_CONFIG_JSON = <<-EOT
    {
      "backend_servers": [{
        "title": "Psono.pw",
          "domain": "${var.USER_DOMAIN}"
      }],
      "allow_custom_server": true,
      "allow_registration": true,
      "allow_lost_password": true,
      "authentication_methods": ["AUTHKEY", "LDAP"]
    }
    EOT
    PSONO_EMAIL_FROM = "noreply@psono.app"
    PSONO_HOST_URL = "https://${var.WEB_DOMAIN}/server"
#    SPLUNK_HOST = ""
#    SPLUNK_PORT = ""
#    SPLUNK_TOKEN = ""
#    SPLUNK_INDEX = ""
#    SPLUNK_PROTOCOL = ""
#    SPLUNK_VERIFY = ""
#    SPLUNK_SOURCETYPE = ""
#    LOGSTASH_TRANSPORT = ""
#    LOGSTASH_HOST = ""
#    LOGSTASH_PORT = ""
#    LOGSTASH_SSL_ENABLED = ""
#    LOGSTASH_SSL_VERIFY = ""
    PSONO_OIDC_CONFIGURATIONS = "{}"
    PSONO_LDAPGATEWAY_EXCLUSIVE_SECRETS = "True"
    PSONO_LDAPGATEWAY = "[]"
    PSONO_COMPLIANCE_DISABLE_UNMANAGED_GROUPS = "True"
    PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_PASSWORD_LENGTH = "17"
    PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_LETTERS_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_LETTERS_LOWERCASE = "abcdefghijklmnopqrstuvwxyz"
    PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_NUMBERS = "0123456789"
    PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_SPECIAL_CHARS = ",.-;:_#+*~!\"§$%&/()=?{[]}"


  }

  site_config {
    application_stack {
      docker_image     = "psono/psono-combo-enterprise"
      docker_image_tag = "latest"
    }
    always_on = "true"

    vnet_route_all_enabled = true
  }

  identity {
    type = "SystemAssigned"
  }

  depends_on = [
    azurerm_postgresql_flexible_server.psono,

  ]
}

resource "azurerm_app_service_connection" "psono" {
  name               = "psono_postgres_connection"
  app_service_id     = azurerm_linux_web_app.psono.id
  target_resource_id = azurerm_postgresql_flexible_server_database.psono.id
  authentication {
    type = "systemAssignedIdentity"
  }
}

