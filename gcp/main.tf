terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.48.0"
    }
  }
}

provider "google" {
  credentials = file("gcp_key.json")

  project = var.GCP_PROJECT_ID
  region  = var.GCP_REGION
  zone    = var.GCP_ZONE
}

resource "google_sql_database_instance" "psono" {
  name             = "psono-instance"
  database_version = "POSTGRES_14"
  region           = var.GCP_REGION

  settings {
    tier = "db-f1-micro"
    availability_type = "REGIONAL"
    backup_configuration {
      enabled = true
    }
  }
  deletion_protection  = "false" # set to true
}

resource "google_sql_database" "psono" {
  name     = var.DATABASES_DEFAULT_DATABASE
  instance = google_sql_database_instance.psono.name
}

resource "google_sql_user" "users" {
  name     = var.DATABASES_DEFAULT_USER
  instance = google_sql_database_instance.psono.name
  password = var.DATABASES_DEFAULT_PASSWORD
}


resource "google_cloud_run_service" "psono" {
  name     = "psono-srv"
  location = var.GCP_REGION

  template {
    spec {
      containers {
        image = "eu.gcr.io/${var.GCP_PROJECT_ID}/psono-combo:latest"
        ports {
            container_port = 80
        }
        env {
          name = "PSONO_SECRET_KEY"
          value = var.SECRET_KEY
        }
        env {
          name = "PSONO_ACTIVATION_LINK_SECRET"
          value = var.ACTIVATION_LINK_SECRET
        }
        env {
          name = "PSONO_DB_SECRET"
          value = var.DB_SECRET
        }
        env {
          name = "PSONO_EMAIL_SECRET_SALT"
          value = var.EMAIL_SECRET_SALT
        }
        env {
          name = "PSONO_PRIVATE_KEY"
          value = var.PRIVATE_KEY
        }
        env {
          name = "PSONO_PUBLIC_KEY"
          value = var.PUBLIC_KEY
        }
        env {
          name = "PSONO_DATABASES_DEFAULT_USER"
          value = var.DATABASES_DEFAULT_USER
        }
        env {
          name = "PSONO_DATABASES_DEFAULT_PASSWORD"
          value = var.DATABASES_DEFAULT_PASSWORD
        }
        env {
          name = "PSONO_DATABASES_DEFAULT_HOST"
          value = "/cloudsql/${google_sql_database_instance.psono.connection_name}"
        }
        env {
          name = "PSONO_DATABASES_DEFAULT_NAME"
          value = var.DATABASES_DEFAULT_DATABASE
        }
        env {
          name = "PSONO_WEB_CLIENT_URL"
          value = "https://${var.WEB_DOMAIN}"
        }
        env {
          name = "PSONO_ALLOWED_DOMAINS"
          value = "*"
        }
        env {
          name = "UWSGI_PROCESSES"
          value = "1"
        }
        env {
          name = "PSONO_HOST_URL"
          value = "https://${var.WEB_DOMAIN}/server"
        }
        env {
          name = "PSONO_MANAGEMENT_ENABLED"
          value = "True"
        }
        env {
          name = "PSONO_SAML_CONFIGURATIONS"
          value = ""
        }
        env {
          name = "PSONO_MANAGEMENT_COMMAND_ACCESS_KEY"
          value = var.MANAGEMENT_COMMAND_ACCESS_KEY
        }
        env {
          name = "PSONO_WEBCLIENT_CONFIG_JSON"
          value = <<-EOT
             {
                 "backend_servers": [{
                     "title": "Psono",
                     "domain": "${var.USER_DOMAIN}"
                 }],
                 "allow_custom_server": true,
                 "allow_registration": true,
                 "allow_lost_password": true,
                 "authentication_methods": ["AUTHKEY", "LDAP"],
                 "more_links": [{
                 "href": "https://doc.psono.com/",
                 "title": "DOCUMENTATION",
                 "class": "fa-book"
                 },{
                 "href": "privacy-policy.html",
                 "title": "PRIVACY_POLICY",
                 "class": "fa-user-secret"
                 },{
                 "href": "https://www.psono.com",
                 "title": "ABOUT_US",
                 "class": "fa-info-circle"
                 }]
             }
             EOT
        }
        env {
          name = "PSONO_PORTAL_CONFIG_JSON"
          value = <<-EOT
             {
             "backend_servers": [{
                 "title": "Psono",
                 "domain": "${var.USER_DOMAIN}"
             }],
             "allow_custom_server": true,
             "allow_registration": true,
             "allow_lost_password": true,
             "authentication_methods": ["AUTHKEY", "LDAP"]
             }
             EOT
        }
      }
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale"      = "1000"
        "run.googleapis.com/cloudsql-instances" = google_sql_database_instance.psono.connection_name
        "run.googleapis.com/client-name"        = "terraform"
      }
    }
  }
  autogenerate_revision_name = true

  traffic {
    percent         = 100
    latest_revision = true
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location    = google_cloud_run_service.psono.location
  project     = google_cloud_run_service.psono.project
  service     = google_cloud_run_service.psono.name

  policy_data = data.google_iam_policy.noauth.policy_data
}